//  --------------------------------------------------------------------------
//  __test__: validate.test.js
//  --------------------------------------------------------------------------

const {singleValidate, deepValidate, validate, expectOneToSucceed} = require('./index');

async function expectToThrow(test, callback) {
    try {
        await test();
    } catch (error) {
        callback(error);
        return;
    }

    throw new Error('No error thrown.');
}

test('validate single without providing object path', () => {
    singleValidate(undefined, {type: 'undefined'});
});

test('validate deep without providing object path', () => {
    deepValidate(undefined, {type: 'undefined'});
});

test('validate undefined', () => {
    validate(undefined, {type: 'undefined'});
});

test('validate any undefined', () => {
    validate(undefined, {type: 'any'});
});

test('validate any true boolean', () => {
    validate(true, {type: 'any'});
});

test('validate any false boolean', () => {
    validate(true, {type: 'any'});
});

test('validate true boolean', () => {
    validate(true, {type: 'boolean'});
});

test('validate false boolean', () => {
    validate(false, {type: 'boolean'});
});

test('validate number', () => {
    validate(1337, {type: 'number'});
});

test('validate string', () => {
    validate('string', {type: 'string'});
});

test('validate regexp', () => {
    validate(/something/, {type: 'regexp'});
});

test('validate array', () => {
    validate([], {type: 'array'});
});

test('validate object', () => {
    validate({}, {type: 'object'});
});

test('validate date', () => {
    validate(new Date(), {type: 'date'});
});

test('validate function', () => {
    validate(() => {}, {type: 'function'});
});

test('validate promise', () => {
    validate(new Promise((resolve) => resolve()), {type: 'promise'});
});

test('provide custom initial object path', () => (
    expectToThrow(() => {
        validate([], {type: 'string'}, ['something']);
    }, (error) => {
        expect(error.data[0]).toBe('something');
    })
));

test('ignore optional values', () => {
    validate({emailAddress: 't@hom.as'}, {
        type: 'object',
        properties: {
            emailAddress: {type: 'string'},
            firstName: {type: 'string', optional: true}
        }
    });
});

test('validate deep object', () => {
    validate({
        emailAddress: 't@hom.as',
        meta: {
            name: 'Thomas',
            deeper: {
                something: true
            }
        }
    }, {
        type: 'object',
        properties: {
            emailAddress: {type: 'string'},
            meta: {
                type: 'object',
                properties: {
                    name: {type: 'string'},
                    deeper: {
                        type: 'object',
                        properties: {
                            something: {type: 'boolean'}
                        }
                    }
                }
            }
        }
    });
});

test('validate array indices', () => {
    validate({
        emailAddress: 't@hom.as',
        names: [
            {name: 'Thomas'},
            {name: 'Eivind'}
        ]
    }, {
        type: 'object',
        properties: {
            emailAddress: {type: 'string'},
            names: {
                type: 'array',
                indices: {
                    type: 'object',
                    properties: {
                        name: {type: 'string'}
                    }
                }
            }
        }
    });
});

test('validate any object with expectOneToSucceed', () => {
    validate({someObject: {}}, {
        type: 'object',
        properties: {
            someObject: {
                type: 'any',
                test: (input) => {
                    expectOneToSucceed([
                        () => validate(input, {type: 'object'}),
                        () => validate(input, {type: 'array'})
                    ]);
                }
            }
        }
    });
});

test('validate any array with expectOneToSucceed', () => {
    validate({someObject: []}, {
        type: 'object',
        properties: {
            someObject: {
                type: 'any',
                test: (input) => {
                    expectOneToSucceed([
                        () => validate(input, {type: 'object'}),
                        () => validate(input, {type: 'array'})
                    ]);
                }
            }
        }
    });
});

test('failing: validate deep object', () => (
    expectToThrow(() => {
        validate({
            emailAddress: 't@hom.as',
            meta: {
                name: 'Thomas',
                deeper: {
                    something: 'test'
                }
            }
        }, {
            type: 'object',
            properties: {
                emailAddress: {type: 'string'},
                meta: {
                    type: 'object',
                    properties: {
                        name: {type: 'string'},
                        deeper: {
                            type: 'object',
                            properties: {
                                something: {type: 'boolean'}
                            }
                        }
                    }
                }
            }
        });
    }, (error) => {
        expect(error.code).toBe('type_error');
        expect(error.data[0]).toBe('meta.deeper.something');
        expect(error.data[1]).toBe('boolean');
    })
));

test('failing: provide no schema', () => (
    expectToThrow(() => {
        validate('Thomas');
    }, (error) => {
        expect(error.code).toBe('missing_required_parameters');
        expect(error.data[0]).toBe('schema');
    })
));

test('failing: provide invalid type schema', () => (
    expectToThrow(() => {
        validate('Thomas', []);
    }, (error) => {
        expect(error.code).toBe('type_error');
        expect(error.data[0]).toBe('schema');
        expect(error.data[1]).toBe('object');
    })
));

test('failing: provide unknown schema type', () => (
    expectToThrow(() => {
        validate('Thomas', {type: 'bazinga'});
    }, (error) => {
        expect(error.code).toBe('unknown_schema_type');
        expect(error.data[0]).toBe('schema.type');
        expect(error.data[1]).toBe('bazinga');
    })
));


test('failing: provide string schema with no type', () => (
    expectToThrow(() => {
        validate('hello', {});
    }, (error) => {
        expect(error.code).toBe('missing_required_parameters');
        expect(error.data[0]).toBe('schema.type');
    })
));

test('failing: provide string schema with invalid type type', () => (
    expectToThrow(() => {
        validate('hello', {
            type: []
        });
    }, (error) => {
        expect(error.code).toBe('type_error');
        expect(error.data[0]).toBe('schema.type');
        expect(error.data[1]).toBe('string');
    })
));

test('failing: provide string schema with invalid property', () => (
    expectToThrow(() => {
        validate('hello', {
            type: 'string',
            something: true
        });
    }, (error) => {
        expect(error.code).toBe('invalid_input');
        expect(error.data[0]).toBe('schema.something');
    })
));

test('failing: provide string schema with invalid type test', () => (
    expectToThrow(() => {
        validate('hello', {
            type: 'string',
            test: true
        });
    }, (error) => {
        expect(error.code).toBe('type_error');
        expect(error.data[0]).toBe('schema.test');
        expect(error.data[1]).toBe('function');
    })
));

test('failing: provide string schema with invalid type optional', () => (
    expectToThrow(() => {
        validate('hello', {
            type: 'string',
            optional: 'hello'
        });
    }, (error) => {
        expect(error.code).toBe('type_error');
        expect(error.data[0]).toBe('schema.optional');
        expect(error.data[1]).toBe('boolean');
    })
));

test('failing: provide object schema with invalid property', () => (
    expectToThrow(() => {
        validate({}, {
            type: 'object',
            something: true
        });
    }, (error) => {
        expect(error.code).toBe('invalid_input');
        expect(error.data[0]).toBe('schema.something');
    })
));

test('failing: provide object schema with invalid type properties', () => (
    expectToThrow(() => {
        validate({}, {
            type: 'object',
            properties: true
        });
    }, (error) => {
        expect(error.code).toBe('type_error');
        expect(error.data[0]).toBe('schema.properties');
        expect(error.data[1]).toBe('object');
    })
));

test('failing: provide object schema with invalid type optional', () => (
    expectToThrow(() => {
        validate({}, {
            type: 'object',
            optional: 'beep'
        });
    }, (error) => {
        expect(error.code).toBe('type_error');
        expect(error.data[0]).toBe('schema.optional');
        expect(error.data[1]).toBe('boolean');
    })
));

test('failing: provide object schema with invalid type test', () => (
    expectToThrow(() => {
        validate({}, {
            type: 'object',
            test: 'beep'
        });
    }, (error) => {
        expect(error.code).toBe('type_error');
        expect(error.data[0]).toBe('schema.test');
        expect(error.data[1]).toBe('function');
    })
));

test('failing: provide array schema with invalid property', () => (
    expectToThrow(() => {
        validate([], {
            type: 'array',
            something: true
        });
    }, (error) => {
        expect(error.code).toBe('invalid_input');
        expect(error.data[0]).toBe('schema.something');
    })
));

test('failing: provide array schema with invalid type indices', () => (
    expectToThrow(() => {
        validate([], {
            type: 'array',
            indices: true
        });
    }, (error) => {
        expect(error.code).toBe('type_error');
        expect(error.data[0]).toBe('schema.indices');
        expect(error.data[1]).toBe('object');
    })
));

test('failing: provide array schema with invalid type optional', () => (
    expectToThrow(() => {
        validate([], {
            type: 'array',
            optional: 'boop'
        });
    }, (error) => {
        expect(error.code).toBe('type_error');
        expect(error.data[0]).toBe('schema.optional');
        expect(error.data[1]).toBe('boolean');
    })
));

test('failing: provide array schema with invalid type test', () => (
    expectToThrow(() => {
        validate([], {
            type: 'array',
            test: 'boop'
        });
    }, (error) => {
        expect(error.code).toBe('type_error');
        expect(error.data[0]).toBe('schema.test');
        expect(error.data[1]).toBe('function');
    })
));

test('failing: provide no deep schema', () => (
    expectToThrow(() => {
        validate({
            name: {
                full_name: 'Thomas'
            }
        }, {
            type: 'object',
            properties: {
                full_name: undefined
            }
        });
    }, (error) => {
        expect(error.code).toBe('missing_required_parameters');
        expect(error.data[0]).toBe('schema.full_name');
    })
));

test('failing: provide invalid type deep schema', () => (
    expectToThrow(() => {
        validate({
            name: {
                full_name: 'Thomas'
            }
        }, {
            type: 'object',
            properties: {
                full_name: []
            }
        });
    }, (error) => {
        expect(error.code).toBe('type_error');
        expect(error.data[0]).toBe('schema.full_name');
        expect(error.data[1]).toBe('object');
    })
));

test('failing: provide no schema type in deep schema', () => (
    expectToThrow(() => {
        validate({
            name: {
                full_name: 'Thomas'
            }
        }, {
            type: 'object',
            properties: {
                full_name: {}
            }
        });
    }, (error) => {
        expect(error.code).toBe('missing_required_parameters');
        expect(error.data[0]).toBe('schema.full_name.type');
    })
));

test('failing: provide unknown type in deep schema', () => (
    expectToThrow(() => {
        validate({
            name: {
                full_name: 'Thomas'
            }
        }, {
            type: 'object',
            properties: {
                full_name: {
                    type: 'stringed'
                }
            }
        });
    }, (error) => {
        expect(error.code).toBe('unknown_schema_type');
        expect(error.data[0]).toBe('schema.full_name.type');
        expect(error.data[1]).toBe('stringed');
    })
));

test('failing: provide unknown type in deep schema', () => (
    expectToThrow(() => {
        validate({
            name: {
                full_name: 'Thomas'
            }
        }, {
            type: 'object',
            properties: {
                full_name: {
                    type: 'stringed'
                }
            }
        });
    }, (error) => {
        expect(error.code).toBe('unknown_schema_type');
        expect(error.data[0]).toBe('schema.full_name.type');
        expect(error.data[1]).toBe('stringed');
    })
));

test('failing: provide invalid type optional in deep string schema', () => (
    expectToThrow(() => {
        validate({
            name: {
                full_name: 'Thomas'
            }
        }, {
            type: 'object',
            properties: {
                full_name: {
                    type: 'string',
                    optional: 'test'
                }
            }
        });
    }, (error) => {
        expect(error.code).toBe('type_error');
        expect(error.data[0]).toBe('schema.full_name.optional');
        expect(error.data[1]).toBe('boolean');
    })
));

test('failing: provide invalid type test in deep string schema', () => (
    expectToThrow(() => {
        validate({
            name: {
                full_name: 'Thomas'
            }
        }, {
            type: 'object',
            properties: {
                full_name: {
                    type: 'string',
                    test: 'test'
                }
            }
        });
    }, (error) => {
        expect(error.code).toBe('type_error');
        expect(error.data[0]).toBe('schema.full_name.test');
        expect(error.data[1]).toBe('function');
    })
));

test('failing: provide invalid type properties in deep object schema', () => (
    expectToThrow(() => {
        validate({
            name: {
                full_name: {}
            }
        }, {
            type: 'object',
            properties: {
                full_name: {
                    type: 'object',
                    properties: 'test'
                }
            }
        });
    }, (error) => {
        expect(error.code).toBe('type_error');
        expect(error.data[0]).toBe('schema.full_name.properties');
        expect(error.data[1]).toBe('object');
    })
));

test('failing: provide invalid type optional in deep object schema', () => (
    expectToThrow(() => {
        validate({
            name: {
                full_name: {}
            }
        }, {
            type: 'object',
            properties: {
                full_name: {
                    type: 'object',
                    optional: 'test'
                }
            }
        });
    }, (error) => {
        expect(error.code).toBe('type_error');
        expect(error.data[0]).toBe('schema.full_name.optional');
        expect(error.data[1]).toBe('boolean');
    })
));

test('failing: provide invalid type test in deep object schema', () => (
    expectToThrow(() => {
        validate({
            name: {
                full_name: {}
            }
        }, {
            type: 'object',
            properties: {
                full_name: {
                    type: 'object',
                    test: 'test'
                }
            }
        });
    }, (error) => {
        expect(error.code).toBe('type_error');
        expect(error.data[0]).toBe('schema.full_name.test');
        expect(error.data[1]).toBe('function');
    })
));

test('failing: provide invalid type indices in deep array schema', () => (
    expectToThrow(() => {
        validate({
            name: {
                full_name: []
            }
        }, {
            type: 'object',
            properties: {
                full_name: {
                    type: 'array',
                    indices: 'test'
                }
            }
        });
    }, (error) => {
        expect(error.code).toBe('type_error');
        expect(error.data[0]).toBe('schema.full_name.indices');
        expect(error.data[1]).toBe('object');
    })
));

test('failing: provide invalid type optional in deep array schema', () => (
    expectToThrow(() => {
        validate({
            name: {
                full_name: []
            }
        }, {
            type: 'object',
            properties: {
                full_name: {
                    type: 'array',
                    optional: 'test'
                }
            }
        });
    }, (error) => {
        expect(error.code).toBe('type_error');
        expect(error.data[0]).toBe('schema.full_name.optional');
        expect(error.data[1]).toBe('boolean');
    })
));

test('failing: provide invalid type test in deep array schema', () => (
    expectToThrow(() => {
        validate({
            name: {
                full_name: []
            }
        }, {
            type: 'object',
            properties: {
                full_name: {
                    type: 'array',
                    test: 'test'
                }
            }
        });
    }, (error) => {
        expect(error.code).toBe('type_error');
        expect(error.data[0]).toBe('schema.full_name.test');
        expect(error.data[1]).toBe('function');
    })
));

test('failing: invalid array index', () => (
    expectToThrow(() => {
        validate([
            {name: 'Thomas'},
            {name: []}
        ], {
            type: 'array',
            indices: {
                type: 'object',
                properties: {
                    name: {type: 'string'}
                }
            }
        });
    }, (error) => {
        expect(error.code).toBe('type_error');
        expect(error.data[0]).toBe('input[1].name');
    })
));

test('failing: invalid deep array index', () => (
    expectToThrow(() => {
        validate({
            indices: [
                {name: 'Thomas'},
                {name: []}
            ]
        }, {
            type: 'object',
            properties: {
                indices: {
                    type: 'array',
                    indices: {
                        type: 'object',
                        properties: {
                            name: {type: 'string'}
                        }
                    }
                }
            }
        });
    }, (error) => {
        expect(error.code).toBe('type_error');
        expect(error.data[0]).toBe('indices[1].name');
    })
));

test('failing: missing required value', () => (
    expectToThrow(() => {
        validate({emailAddress: 't@hom.as'}, {
            type: 'object',
            properties: {
                emailAddress: {type: 'string'},
                firstName: {type: 'string'}
            }
        });
    }, (error) => {
        expect(error.code).toBe('missing_required_parameters');
    })
));

test('failing: superfluous values', () => (
    expectToThrow(() => {
        validate({
            emailAddress: 't@hom.as',
            firstName: 'Thomas'
        }, {
            type: 'object',
            properties: {
                emailAddress: {type: 'string'}
            }
        });
    }, (error) => {
        expect(error.code).toBe('invalid_input');
        expect(error.data[0]).toBe('firstName');
    })
));

test('failing: invalid type', () => (
    expectToThrow(() => {
        validate('hello', {type: 'bazinga'});
    }, (error) => {
        expect(error.code).toBe('unknown_schema_type');
    })
));

test('failing: type error', () => (
    expectToThrow(() => {
        validate('hello', {type: 'object'});
    }, (error) => {
        expect(error.code).toBe('type_error');
    })
));

test('failing: invalid custom test type', () => (
    expectToThrow(() => {
        validate({name: 'Thomas'}, {
            type: 'object',
            properties: {
                name: {
                    type: 'string',
                    test: 'something'
                }
            }
        });
    }, (error) => {
        expect(error.code).toBe('type_error');
        expect(error.data[0]).toBe('schema.name.test');
    })
));

test('failing: failing custom test', () => (
    expectToThrow(() => {
        validate({name: 'Thomas'}, {
            type: 'object',
            properties: {
                name: {
                    type: 'string',
                    test: (value) => {
                        if (value.length < 10) {
                            throw new Error('Invalid length');
                        }
                    }
                }
            }
        });
    }, (error) => {
        expect(error.message).toBe('Invalid length');
    })
));

test('failing: failing custom test and provide object path', () => (
    expectToThrow(() => {
        validate({name: 'Thomas'}, {
            type: 'object',
            properties: {
                name: {
                    type: 'string',
                    test: (value, objectPath) => {
                        if (value.length < 10) {
                            throw new Error(objectPath);
                        }
                    }
                }
            }
        });
    }, (error) => {
        expect(error.message).toBe('name');
    })
));

test('failing: expectOneToSucceed without arguments', () => (
    expectToThrow(() => {
        expectOneToSucceed();
    }, (error) => {
        expect(error.type).toBe('missing_required_parameters');
        expect(error.data[0]).toBe('input');
    })
));

test('failing: expectOneToSucceed with invalid type input', () => (
    expectToThrow(() => {
        expectOneToSucceed({});
    }, (error) => {
        expect(error.type).toBe('type_error');
        expect(error.data[0]).toBe('input');
        expect(error.data[1]).toBe('array');
    })
));

test('failing: expectOneToSucceed with invalid type input index', () => (
    expectToThrow(() => {
        expectOneToSucceed(['test']);
    }, (error) => {
        expect(error.type).toBe('type_error');
        expect(error.data[0]).toBe('input[0]');
        expect(error.data[1]).toBe('function');
    })
));

test('failing: validate any array with expectOneToSucceed', () => (
    expectToThrow(() => {
        validate({someObject: 'this is test'}, {
            type: 'object',
            properties: {
                someObject: {
                    type: 'any',
                    test: (input) => {
                        try {
                            expectOneToSucceed([
                                () => validate(input, {type: 'object'}),
                                () => validate(input, {type: 'array'})
                            ]);
                        } catch (error) {
                            throw new Error('stuff happened');
                        }
                    }
                }
            }
        });
    }, (error) => {
        expect(error.message).toBe('stuff happened');
    })
));
