const isUndefined = require('@amphibian/is-undefined');
const isBoolean = require('@amphibian/is-boolean');
const isNumber = require('@amphibian/is-number');
const isString = require('@amphibian/is-string');
const isRegExp = require('@amphibian/is-regexp');
const isArray = require('@amphibian/is-array');
const isObject = require('@amphibian/is-object');
const isDate = require('@amphibian/is-date');
const isFunction = require('@amphibian/is-function');
const isPromise = require('@amphibian/is-promise');
const iterateUpArray = require('@amphibian/iterate-up-array');
const forOwn = require('@amphibian/for-own');
const objectHasProperty = require('@amphibian/object-has-property');
const errors = require('@amphibian/errors');

const testers = {
    any: () => true,
    undefined: isUndefined,
    boolean: isBoolean,
    number: isNumber,
    string: isString,
    regexp: isRegExp,
    array: isArray,
    object: isObject,
    date: isDate,
    function: isFunction,
    promise: isPromise
};

const defaultSchema = {
    __skip_validation__: true,
    type: 'object',
    properties: {
        type: {__skip_validation__: true, type: 'string'},
        optional: {__skip_validation__: true, type: 'boolean', optional: true},
        test: {__skip_validation__: true, type: 'function', optional: true}
    }
};

const objectSchema = {
    __skip_validation__: true,
    type: 'object',
    properties: {
        type: {__skip_validation__: true, type: 'string'},
        properties: {__skip_validation__: true, type: 'object', optional: true},
        optional: {__skip_validation__: true, type: 'boolean', optional: true},
        test: {__skip_validation__: true, type: 'function', optional: true}
    }
};

const arraySchema = {
    __skip_validation__: true,
    type: 'object',
    properties: {
        type: {__skip_validation__: true, type: 'string'},
        indices: {__skip_validation__: true, type: 'object', optional: true},
        optional: {__skip_validation__: true, type: 'boolean', optional: true},
        test: {__skip_validation__: true, type: 'function', optional: true}
    }
};

/**
 * Validate single input
 * @param {any} input
 * @param {object} schema
 * @param {array} objectPath
**/
function singleValidate(input, schema, objectPath = []) {
    const objectPathString = objectPath.join('.') || 'input';
    const {type, optional} = schema;

    if (!objectHasProperty(testers, type)) {
        throw errors.invalidInput('unknown_schema_type', ['schema'].concat(objectPath, 'type').join('.'), type);
    }

    if ((optional === true) && isUndefined(input)) {
        return true;
    }

    if (isUndefined(input) && (type !== 'undefined') && (type !== 'any')) {
        throw errors.missingRequiredParameters(null, objectPathString);
    } else if (!testers[type](input)) {
        throw errors.typeError(null, objectPathString, type);
    }

    if (objectHasProperty(schema, 'test')) {
        schema.test(input, objectPathString, objectPath);
    }

    return true;
}

/**
 * Validate deeply
 * @param {any} input
 * @param {object} schema
 * @param {array} objectPath
**/
function deepValidate(input, schema, objectPath = []) {
    if (input && input.__skip_validation__) {
        return true;
    }

    // Validate the schemas before doing validating against them
    switch (schema && schema.type) {
        case 'object': {
            deepValidate(schema, objectSchema, ['schema'].concat(objectPath));
            break;
        }

        case 'array': {
            deepValidate(schema, arraySchema, ['schema'].concat(objectPath));
            break;
        }

        default: {
            deepValidate(schema, defaultSchema, ['schema'].concat(objectPath));
        }
    }

    singleValidate(input, schema, objectPath);

    // Make sure there's input before iterating on it
    if (input) {
        const {type} = schema;

        if (type === 'object') {
            if (objectHasProperty(schema, 'properties')) {
                forOwn(schema.properties, (key, subSchema) => {
                    deepValidate(input[key], subSchema, objectPath.concat(key));
                });

                forOwn(input, (key) => {
                    if (!objectHasProperty(schema.properties, key)) {
                        throw errors.invalidInput(null, objectPath.concat(key).join('.'));
                    }
                });
            }
        } else if (type === 'array') {
            if (objectHasProperty(schema, 'indices')) {
                iterateUpArray(input, (index, i) => {
                    const copiedPath = objectPath.slice();
                    const copiedPathLength = copiedPath.length;

                    if (copiedPathLength > 0) {
                        copiedPath[copiedPathLength - 1] += `[${i}]`;
                    } else {
                        copiedPath[0] = `input[${i}]`;
                    }

                    deepValidate(index, schema.indices, copiedPath);
                });
            }
        }
    }

    return true;
}

/**
 * Validate
 * @param {any} input
 * @param {object} schema
 * @param {array} objectPath
**/
function validate(input, schema, objectPath) {
    return deepValidate(input, schema, objectPath);
}

const expectOneToSucceedSchema = {
    type: 'array',
    indices: {type: 'function'}
};

/**
 * Expect one to succeed
 * @param {array} input
**/
function expectOneToSucceed(input) {
    validate(input, expectOneToSucceedSchema);

    let thrownError = null;
    let oneDidSucceed = false;

    iterateUpArray(input, (test, i, end) => {
        try {
            test();
            oneDidSucceed = true;
            end();
        } catch (error) {
            thrownError = error;
        }
    });

    if (oneDidSucceed) {
        return true;
    }

    throw thrownError;
}

exports.default = validate;
module.exports = {
    singleValidate,
    deepValidate,
    validate,
    expectOneToSucceed
};
